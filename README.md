# Powershell-Scripts

*  **Search-ADUser**: Like Get-ADUser, but simplier.
*  **Get-ADEasyAdmins**: Get easy passwords for AD users within admin AD groups.
*  **ConvertTo-UnsecureString**: Convert Powershell SecureString to String. Useful for decrypting passwords.
*  **PAFS**: Search strings within Office documents (Word and Excel).
