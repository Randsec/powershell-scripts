﻿# Parse And Find String

#Requires -Modules ImportExcel, PSWriteWord

$ErrorActionPreference = 'SilentlyContinue'
$WarningPreference = 'SilentlyContinue'

$searchthis = "CADENAUNICA"
$roots = get-psdrive -PSProvider 'FileSystem' | select-object -property "Root"
$docFiles = @()
$excFiles = @()

foreach ($root in $roots) {
    $disk = $root.Root
    write-host "[i] Buscando en disco: $disk"
    $suspectFiles = get-childitem -path $disk -recurse -Include "*.docx","*.doc","*.xls","*.xlsx" | % {$_.FullName}

    foreach ($file in $suspectFiles) {
        if ($file -like "*.doc*") {
            $docFiles += ,$file
        }
        elseif ($file -like "*.xls*") {
            $excFiles += ,$file
        }
    }
}

# XLS parse
foreach ($file in $excFiles) {
    $content = Import-Excel -path $file -NoHeader
    if ($content | Select-String -Pattern $searchthis) {
        write-host "[!!] Texto encontrado en: $file" -ForegroundColor Green

    }
}

# DOC parse
foreach ($file in $docFiles) {
    $word = Get-WordDocument -FilePath $file
    $content = $word.Text
    if ($content | Select-String -Pattern $searchthis) {
        write-host "[!!] Texto encontrado en: $file" -ForegroundColor Cyan
    }
}
