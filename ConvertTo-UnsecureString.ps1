<#.EXAMPLE
PS > ConvertTo-UnsecureString -SecPassword <System.Security.SecureString>
#>

function ConvertTo-UnsecureString() {
    [CmdletBinding()] param (
        [Parameter(Mandatory=$True)]
        [ValidateNotNullOrEmpty()]
        [Security.SecureString]$SecPassword = $(Throw "SecureString Password required")
    )

    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecPassword)
    $UnsecurePassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    $UnsecurePassword
}