param
(
[string]$SurName="*",
[string]$GivenName="*",
[string]$Name="*"

)

Get-ADUser -Filter {(SurName -like $SurName) -and (GivenName -like $GivenName) -and (Name -like $Name)}